all_p:	client_ server_proccess

all_t: client_ server_thread

client_: client/client.c
	gcc -Wall client/client.c -o client/client

server_proccess: server/server.c
	gcc -Dproccess server/server.c -o server/server

server_thread: server/server.c
	gcc -Dthread server/server.c -D_REENTERANT -I/usr/include/nptl -L/usr/lib/nptl -lpthread -o server/server

clean:
	rm -f  server/server client/client
