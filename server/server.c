#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>


void error(char *msg)
{
  perror(msg);
  exit(1);
}

typedef struct _params{
	int newsock_fd;
}params;

void* get_file(void *arg)
{
    	char buffer[256];
	int n;		//number of characters read or written
	int h_file, read_file, close_file;
	int i;

	params * param = (params*)arg;

	bzero(buffer,256);

	n = read(param->newsock_fd, buffer, 255);
	h_file = open(buffer, O_RDWR, 0077);
	if(h_file < 0)
	{
		close(param->newsock_fd);	
		error("ERROR opening file");
	}
	for(i = 0; ; i++){
		bzero(buffer,256);
		read_file = pread(h_file, buffer, 255, 255*i);
		if(read_file < 0)
			error("ERROR reading file");

		n = write(param->newsock_fd, buffer, strlen(buffer));
		if (n < 0) 
			error("ERROR writing to socket");
		if(read_file == 0)
			break;
	}

	close_file = close(h_file);
	if(close_file < 0)
		error("ERROR closing file");

	n = write(param->newsock_fd, "", 0);
	if (n < 0) 
		error("ERROR writing to socket");

	close(param->newsock_fd);
}

int main(int argc, char *argv[])
{
	int sock_fd, newsock_fd, port_number, client_len; //socket file descriptor, new coskfd, port, size of the address of the client
	struct sockaddr_in server_addr, client_addr;	//internet address
	
	
	//int rc;

	if (argc < 2) 
   		error("ERROR, no port provided");
 
	port_number = atoi(argv[1]);

	sock_fd = socket(AF_INET, SOCK_STREAM, 0);	//address domain of the socket, type, 
	if (sock_fd < 0)
  		error("ERROR opening socket");
	bzero((char *) &server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port_number);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	if (bind(sock_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
  		error("ERROR on binding");
	listen(sock_fd,5);
	client_len = sizeof(client_addr);
	for (;;) {	
		newsock_fd = accept(sock_fd, (struct sockaddr *)&client_addr, (socklen_t*)&client_len);
		if (newsock_fd < 0)
	  		error("ERROR on accept");

		#ifdef proccess
		{
			pid_t pid;
			switch(pid = fork()) {
		  		case -1:
			  		error("Fork");
		  		case 0:		//child
					close(sock_fd);
					params param;
					param.newsock_fd = newsock_fd;
					get_file(&param);
			  		exit(0);
		  		default:	//parent
					close(newsock_fd);
			  		break;
		  	}
		}

		#else
		#ifdef thread
		{	
			pthread_t mythread;
			int rc;
			params param;
			param.newsock_fd = newsock_fd;
			if (rc = pthread_create(&mythread, NULL, get_file, (void*)&param) != 0)
		 		error("ERROR; pthread_create()");
		}
		#endif
		#endif
	}
	
	return 0;
}
